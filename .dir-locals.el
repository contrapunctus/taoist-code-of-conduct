;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-html-postamble . nil)
              (org-export-with-section-numbers . nil)
              (org-export-with-toc . nil)
              (org-html-self-link-headlines . nil)
              (org-html-head . "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />")
              (eval . (require 'ox-tufte))
              (eval . (add-hook 'after-save-hook
                                (lambda ()
                                  (org-tufte-export-to-file))
                                0 t)))))
